package bo.com.relativity.ejercicioviewsa;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    RadioGroup rgOpciones;
    RadioButton rbMayuscula, rbMinuscula;
    EditText etDato;
    TextView tvInformacion;
    Button btnMostrar;

    CheckBox cbTransparencia, cbTinte, cbRedimension;
    ImageView imageView;

    Switch swMostrarWeb;
    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        inicializarVistas();
    }

    private void inicializarVistas() {
        //PRIMER PANEL
        rgOpciones = findViewById(R.id.rgOpciones);
        rbMayuscula = findViewById(R.id.rbMayuscula);
        rbMinuscula = findViewById(R.id.rbMinuscula);
        etDato = findViewById(R.id.etDato);
        tvInformacion = findViewById(R.id.tvInformacion);
        btnMostrar = findViewById(R.id.btnMostrar);

        //limpia todas las opciones seleccionadas de su radio grup
        rgOpciones.clearCheck();
        rgOpciones.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                accionRadioButon(checkedId);
            }
        });

        btnMostrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                accionBoton();
            }
        });


        //SEGUNDO PANEL
        cbTransparencia = findViewById(R.id.cbTransparencia);
        cbTinte = findViewById(R.id.cbTinte);
        cbRedimension = findViewById(R.id.cbRedimension);
        imageView = findViewById(R.id.imageView);

        // configurar la accion check de un checkbox para la transparencia
        cbTransparencia.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                cambiarCanalAlfa(isChecked);
            }
        });

        // configurar la accion check de un checkbox para un filtro de color
        cbTinte.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                cambiarTinte(isChecked);
            }
        });

        //configurar la accion check del checbox para redimensionar la imagen
        cbRedimension.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                cambiarTamanio(isChecked);
            }
        });


        //TERCER PANEL
        //configurar el evento que escuchará un Switch
        swMostrarWeb = findViewById(R.id.swMostrarWeb);
        webView = findViewById(R.id.webView);

        //configurar el cargado de una url en el WebView
        webView.loadUrl("https://www.google.com");
        //como hacer que un View maneje su visibilidad.
        webView.setVisibility(View.INVISIBLE);
        //swMostrarWeb.setVisibility(View.INVISIBLE);

        swMostrarWeb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mostrarWeb(isChecked);
            }
        });


    }

    private void mostrarWeb(boolean isChecked) {
        // como hacer visible el contenedor de la WebView (url)
        if(isChecked){
            webView.setVisibility(View.VISIBLE);
        } else{
            webView.setVisibility(View.INVISIBLE);
        }
    }

    private void cambiarTamanio(boolean isChecked) {
        // trabajar en redimensionar el size
        // de una imagen
        if(isChecked) {
            imageView.setScaleX(2);
            imageView.setScaleY(2);
        } else {
            imageView.setScaleX(1);
            imageView.setScaleY(1);
        }
    }

    private void cambiarTinte(boolean isChecked) {
        // trabajar en aplicar un filtro de color
        // a la imagen en cuestion
        if(isChecked){
            imageView.setColorFilter(Color.argb(150,255,120,0));
        } else {
            imageView.setColorFilter(Color.argb(0,0,0,0));
        }
    }

    private void cambiarCanalAlfa(boolean isChecked) {
        //La transparencia representa
        //cambiar el canal Alfa de una imagen
        if(isChecked) {
            imageView.setAlpha(0.4f);
        } else {
            imageView.setAlpha(1.0f);
        }
    }

    private void accionBoton() {
        String dato = etDato.getText().toString().trim();
        tvInformacion.setText(dato);
    }

    private void accionRadioButon(int checkedId) {
       RadioButton rb = findViewById(checkedId);
       String info = "";
       info = tvInformacion.getText().toString();
       switch (rb.getId()){
           case R.id.rbMayuscula:
               tvInformacion.setText(info.toUpperCase());
               break;
           case R.id.rbMinuscula:
               tvInformacion.setText(info.toLowerCase());
               break;
       }
    }
}
